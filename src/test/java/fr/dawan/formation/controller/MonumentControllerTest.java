package fr.dawan.formation.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.dawan.formation.RestPageImpl;
import fr.dawan.formation.domain.Monument;
import fr.dawan.formation.domain.Point;
import fr.dawan.formation.exception.MonumentNotFoundException;
import fr.dawan.formation.service.MonumentService;
import net.sf.ehcache.CacheManager;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class MonumentControllerTest {
    private final static Monument TESTING_MONUMENT_INSTANCE = new Monument(
            1,
            new String("Tour Eiffel"),
            "La célèbre tour de Paris",
            Arrays.asList(new Point(1, 15.3, 18.6), new Point(2, 15.9, 17.5))
    );
    @MockBean
    private MonumentService monumentService;

    @MockBean
    private CacheManager cacheManager;

    @Autowired
    private MockMvc mvc;

    private JacksonTester<Monument> json;
    private JacksonTester<List<Monument>> jsonMonumentListTester;
    //private JacksonTester<Page<Monument>> jsonMonumentPageTester;
    private JacksonTester<RestPageImpl<Monument>> jsonMonumentPageTester;


    @Before
    public void setUp(){
        JacksonTester.initFields(this, new ObjectMapper());
    }

    @Test
    public void getOneTest() throws Exception {
        // given
        given(monumentService.findById(anyLong())).willReturn(TESTING_MONUMENT_INSTANCE);

        // when
        MockHttpServletResponse response = mvc.perform(
                get("/monument/1").accept("application/json;charset=UTF-8")).andReturn().getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo(json.write(TESTING_MONUMENT_INSTANCE).getJson());
        verify(monumentService, times(1)).findById(anyLong());
    }
    @Test
    public void getOneFailTest() throws Exception {
        // given
        given(monumentService.findById(anyLong())).willThrow(MonumentNotFoundException.class);

        // when
        MockHttpServletResponse response = mvc.perform(
                get("/monument/1").accept("application/json;charset=UTF-8"))
                .andExpect(status().isNotFound())
                .andReturn()
                .getResponse();

        // then
        verify(monumentService, times(1)).findById(anyLong());
    }


    @Test
    public void deleteTest() throws Exception {
        // given
        given(monumentService.delete(anyLong())).willReturn(true);
        given(monumentService.findById(1)).willReturn(TESTING_MONUMENT_INSTANCE);

        // when
        MockHttpServletResponse response = mvc.perform(
                delete("/monument/1")).andExpect(status().isOk()).andReturn().getResponse();
        // then
        assertThat(response.getContentAsString()).isEqualTo("true");
        verify(monumentService, times(1)).delete(anyLong());
        verify(monumentService, times(1)).findById(anyLong());
    }
    @Test
    public void postOneTest() throws Exception {
        // given
        given(monumentService.save(any(Monument.class)))
                .willReturn(TESTING_MONUMENT_INSTANCE);

        // when
        MvcResult result = mvc.perform(post("/monument")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.write(TESTING_MONUMENT_INSTANCE).getJson()))
                .andExpect(status().isCreated())
                .andReturn();

        // then

        assertThat(result.getResponse().getHeader("location")).isEqualTo("http://localhost/monument/"
                +TESTING_MONUMENT_INSTANCE.getId());
        verify(monumentService, times(1)).save(any(Monument.class));
    }
    @Test(expected = Exception.class)
    public void postOneFailTest() throws Exception {
        //given
        given(monumentService.findByNom("Tour Eiffel")).willReturn(Optional.of(TESTING_MONUMENT_INSTANCE));

         // when
        mvc.perform(post("/monument")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.write(TESTING_MONUMENT_INSTANCE).getJson()))
                .andExpect(status().isConflict())
                .andReturn();
        verify(monumentService, times(1)).findByNom(" was called one time");
    }
    @Test
    public void putOneTest() throws Exception {
        // given
        given(monumentService.save(any(Monument.class)))
                .willReturn(TESTING_MONUMENT_INSTANCE);
        given(monumentService.findById(1)).willReturn(TESTING_MONUMENT_INSTANCE);

        // when
        MvcResult result = mvc.perform(put("/monument")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.write(TESTING_MONUMENT_INSTANCE).getJson()))
                .andExpect(status().isCreated())
                .andReturn();

        // then
        assertThat(result.getResponse().getHeader("location")).isEqualTo("http://localhost/monument/"
                +TESTING_MONUMENT_INSTANCE.getId());
        verify(monumentService, times(1)).save(any(Monument.class));
        verify(monumentService, times(1)).findById(1);
    }
    @Test
    public void getAllTest() throws Exception {
        // given
        List<Monument> expectedMonuments = new ArrayList<>();
        expectedMonuments.add(new Monument(1, new String("Tour Eiffel"), "La célèbre tour de Paris", Arrays.asList(new Point(1,15.3, 18.6), new Point(2, 15.9,17.5))));
        expectedMonuments.add(new Monument(2, new String("Arc de triomphe"), "La victoire ou la mort", Arrays.asList(new Point(3,85, 185.6), new Point( 4,60.3,178.5))));
        expectedMonuments.add(new Monument(3, new String("Muraille de chine"), "Fort Alamo", Arrays.asList(new Point(5,43, 67), new Point(6, 86.9,74.5))));
        List<Monument> testedMonuments = new ArrayList<>();
        given(monumentService.findAll()).willReturn(expectedMonuments);

        // when
        MvcResult result = mvc.perform(get("/monument/all").accept("application/json;charset=UTF-8"))
                .andExpect(status().isOk())
                .andReturn();
        // then
        assertThat(result.getResponse().getContentAsString()).isEqualTo(jsonMonumentListTester.write(expectedMonuments).getJson());
        verify(monumentService, times(1)).findAll();
    }
    @Test
    public void pagingAllTest() throws Exception {
        // given
        List<Monument> expectedMonuments = new ArrayList<>();
        expectedMonuments.add(new Monument(1, new String("Tour Eiffel"), "La célèbre tour de Paris", Arrays.asList(new Point(1,15.3, 18.6), new Point(2, 15.9,17.5))));
        expectedMonuments.add(new Monument(2, new String("Arc de triomphe"), "La victoire ou la mort", Arrays.asList(new Point(3,85, 185.6), new Point( 4,60.3,178.5))));
        expectedMonuments.add(new Monument(3, new String("Muraille de chine"), "Fort Alamo", Arrays.asList(new Point(5,43, 67), new Point(6, 86.9,74.5))));

        // Utilisation d'une implémentation spécifique
        RestPageImpl<Monument> monumentPage = new RestPageImpl<>(expectedMonuments);
        Pageable p = PageRequest.of(1,3);
        given(monumentService.findAll(p)).willReturn(monumentPage);

        // when

        mvc.perform(get("/monument?page=1&size=3").accept("application/json;charset=UTF-8"))
                .andExpect(status().isOk())
                /*
                 * Transformer le résulat de la requête en chaîne de caractère grâce à une reproduction de PageImpl
                 * pour le moment on vérifie simplement si la chaîne littérale est identique
                 * Peut-être il faudra plutôt utiliser restTemplate comme pour les tests d'intégration sans charger tout
                 * le contexte de l'appli ?
                 */
                .andExpect(content().string(jsonMonumentPageTester.write(monumentPage).getJson()))
                .andReturn();
        verify(monumentService, times(1)).findAll(p);
    }

}
