package fr.dawan.formation.repository;

import fr.dawan.formation.domain.Format;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormatRepository extends PagingAndSortingRepository<Format,String> {

	public Page<Format> findAll(Pageable p);
	
}
