package fr.dawan.formation.domain;

import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * Cette classe représente le monument
 */
@RequiredArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
@Entity
@Table
@AllArgsConstructor
public final class Monument {
    @Id
    private long id;

    private String nom;
    private String description;

    @OneToMany(cascade = CascadeType.MERGE)
    private List<Point> position;
}
