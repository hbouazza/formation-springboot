package fr.dawan.formation.service;

import fr.dawan.formation.domain.Monument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface MonumentService {
    /**
     * @return a Monument
     */
   Monument findById(long id) throws Exception;
    /**
     * @return a Searched Monument
     */
    Optional<Monument> findByNom(String nom) throws Exception;

    /**
     * @return a Monument List
     */
    List<Monument> findAll() throws Exception;
    /**
     * @return a Monument Page
     */
    Page<Monument> findAll(Pageable p) throws Exception;

    /**
     * @return a saved monument
     */
    Monument save(Monument monument) throws Exception;

    /**
     * @return a boolean
     */
    Boolean delete(long id) throws Exception;

 /**
  * insérer données de Test
  */
 void initData();
}
