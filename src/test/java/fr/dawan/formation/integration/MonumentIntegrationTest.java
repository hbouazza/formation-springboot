package fr.dawan.formation.integration;

import fr.dawan.formation.Application;
import fr.dawan.formation.domain.Monument;
import fr.dawan.formation.domain.Point;
import fr.dawan.formation.repository.MonumentRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("TEST")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class MonumentIntegrationTest {

    @Autowired
    private MonumentRepository monumentRepository;

    private TestRestTemplate restTemplate = new TestRestTemplate();

    @Before
    public void emptyDataBase(){
        monumentRepository.deleteAll();
    }

    @Test
    public void testGetService(){
        // given
        Monument tour =  new Monument(1,"Tour Eiffel", "La célèbre tour de Paris",
                Arrays.asList(new Point(1,15.3, 18.6), new Point(2, 15.9,17.5)));

        tour = monumentRepository.save(tour);

        // when
        ResponseEntity<Monument> response = restTemplate.exchange(
                "http://localhost:8080/monument/" + tour.getId(), HttpMethod.GET, null, Monument.class);

        Monument testedMonument = response.getBody();

        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertEquals( tour.getNom(), testedMonument.getNom());
        assertEquals( tour.getDescription(), testedMonument.getDescription());
    }



}
