package fr.dawan.formation.repository;

import fr.dawan.formation.domain.Image;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ImageRepository extends CrudRepository<Image,String>{
	@Query("FROM Image i WHERE i.imageGroupId=:groupId AND i.format.nom=:formatName")
	public Image findByImageGroupId(@Param("groupId") String groupId, @Param("formatName") String formatName);
}
