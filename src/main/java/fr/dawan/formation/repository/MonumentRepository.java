package fr.dawan.formation.repository;

import fr.dawan.formation.domain.Monument;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface MonumentRepository extends CrudRepository<Monument,Long> {

    boolean existsByNom(String nom);

    Optional<Monument> findByNom(String nom);

    @Cacheable("test")
    Page<Monument> findAll(Pageable p);

    // HQL = Hibernate Query Language nécessaire parfois
    // ici si on a pas de description on va chercher tous les monuments
    @Query("SELECT m FROM Monument m WHERE m.description = null OR m.description LIKE %:description%")
    List<Monument> findByOptionalDescriptionAndOptionalPositionANdByOptional(Optional<String> description);


}
