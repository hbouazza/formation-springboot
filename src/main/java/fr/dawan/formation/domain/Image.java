package fr.dawan.formation.domain;

import javax.persistence.*;


@Entity
@Table
public class Image {
	
	@Id
	private String id;

	@Column(columnDefinition="LONGBLOB")
	private byte[] contenu;
	
	@ManyToOne
	private Format format;

	private String originalFileName;

	private String imageGroupId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public byte[] getContenu() {
		return contenu;
	}
	public void setContenu(byte[] contenu) {
		this.contenu = contenu;
	}
	public Format getFormat() {
		return format;
	}
	public void setFormat(Format format) {
		this.format = format;
	}
	public String getOriginalFileName() {
		return originalFileName;
	}
	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}
	public String getImageGroupId() {
		return imageGroupId;
	}
	public void setImageGroupId(String imageGroupId) {
		this.imageGroupId = imageGroupId;
	}
	
	
}
