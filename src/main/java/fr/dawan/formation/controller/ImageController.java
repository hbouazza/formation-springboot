package fr.dawan.formation.controller;


import fr.dawan.formation.domain.Format;
import fr.dawan.formation.domain.Image;
import fr.dawan.formation.repository.FormatRepository;
import fr.dawan.formation.repository.ImageRepository;
import fr.dawan.formation.service.TransformerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/image")
public class ImageController {

	@Autowired
	private
	TransformerService transformer;

	@Autowired
	private
	FormatRepository formatRepository;
	
	@Autowired
	private
	ImageRepository imageRepository;
	
	@RequestMapping(value="",
			        method=RequestMethod.POST,
				    consumes="multipart/form-data")
	String upload(@RequestParam(value = "fileUpload", required = true) 
					MultipartFile file) throws IOException, InterruptedException {

	   Image img = new Image();

	   img.setContenu(file.getBytes());
	   img.setOriginalFileName(file.getOriginalFilename());
	   img.setFormat(null);
	   img.setImageGroupId(img.getId());
	   
	   imageRepository.save(img);
		
	   for (Format format : formatRepository.findAll()) {
		   Image transformed = transformer.transform(img, format);
		   imageRepository.save(transformed);   
	   }
		
	   return img.getImageGroupId();
	   
	}

	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	void delete(@PathVariable String id){

	}
	
	@RequestMapping(value="/{id}", 
					method=RequestMethod.GET, 
					produces="application/octet-stream")
	ResponseEntity<byte[]> get(@PathVariable String id, 
				@RequestParam("format") String format){
		Format f = formatRepository.findById(format).get();
		Image img = imageRepository.findByImageGroupId(id,format);

		return ResponseEntity.ok()
			.header("Content-Type",f.getMimeType())
			.header("Content-Disposition", 
					"attachment; filename=\"out"+f.getExtension()+"\"")
			.body(img.getContenu());
	}
	
}
