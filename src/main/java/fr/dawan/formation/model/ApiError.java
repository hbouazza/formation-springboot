package fr.dawan.formation.model;

public class ApiError {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ApiError() {
    }
}
