package fr.dawan.formation.domain;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table
@XmlRootElement
public class Format {
	@Id
	private String nom;
	private String extension;
	private String mimeType;
	private HashMap<String,String> proprietes = new HashMap<>();

	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getMimeType() {
		return mimeType;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	public HashMap<String, String> getProprietes() {
		return proprietes;
	}
	public void setProprietes(HashMap<String, String> proprietes) {
		this.proprietes = proprietes;
	}
	
	
}
