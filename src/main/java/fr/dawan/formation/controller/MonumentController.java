package fr.dawan.formation.controller;

import fr.dawan.formation.domain.Monument;
import fr.dawan.formation.service.MonumentService;
import net.sf.ehcache.CacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/monument")
public class MonumentController {


    @Autowired
    private CacheManager cacheManager;

    private final MonumentService monumentService;

    @Autowired
    public MonumentController(MonumentService monumentService, CacheManager cacheManager) {
        this.monumentService = monumentService;
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<Monument>> getAll() throws Exception {

        return ResponseEntity.ok(monumentService.findAll());
    }

    @GetMapping(value = "/insert-data")
    public void data() throws Exception {
        monumentService.initData();
    }
    @GetMapping
    public ResponseEntity<Page<Monument>> getPage(Pageable p) throws Exception {
        cacheManager.getCache("test");
        return ResponseEntity.ok(monumentService.findAll(p));
    }

    @GetMapping(value="/{id}")
    ResponseEntity<Monument> getOne(@PathVariable("id") int id) throws Exception {
        Monument m = monumentService.findById(id);
        if (m == null){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(m);
    }


    @PostMapping
    public ResponseEntity<Void> postNew(@RequestBody Monument monument) throws Exception {
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(monument.getId())
                .toUri();
        Monument addedMonument = monumentService.save(monument);

        if (addedMonument == null)
            return ResponseEntity.noContent().build();



        return ResponseEntity.created(location).build();
    }
    @PutMapping
    public ResponseEntity<Void> update(@RequestBody Monument monument) throws Exception {
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(monument.getId())
                .toUri();
        Monument searchingMonument = monumentService.findById((int)monument.getId());
        if (searchingMonument == null){
            return ResponseEntity.noContent().location(location).build();
        }
        Monument updatedMonument = monumentService.save(monument);

        if (updatedMonument == null)
            return ResponseEntity.noContent().build();

        return ResponseEntity.created(location).build();
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Boolean> deleteOne(@PathVariable("id") long id) throws Exception {
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(id)
                .toUri();
        Monument searchingMonument = monumentService.findById(id);

        if (searchingMonument == null){
            return ResponseEntity.noContent().location(location).build();
        }

        boolean result = monumentService.delete(id);

        if (!result)
            return ResponseEntity.noContent().location(location).build();

        return ResponseEntity.ok(result);
    }

    @RequestMapping(value="/image",method=RequestMethod.POST,consumes="multipart/form-data")
    String upload(@RequestParam(value = "fileUpload", required = true) MultipartFile file) throws IOException {
        return "uploaded"+file.getBytes().length;
    }
}
