package fr.dawan.formation.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@RequiredArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
@Entity
@Table
@AllArgsConstructor
public final class Point {

    @Id
    private long id;
    private double latitude;
    private double longitude;
}