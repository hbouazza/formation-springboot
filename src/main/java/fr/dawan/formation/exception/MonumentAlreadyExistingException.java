package fr.dawan.formation.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class MonumentAlreadyExistingException extends RuntimeException {

    private static String MESSAGE = "Le monument [%s] existe déjà !";
    public MonumentAlreadyExistingException(String nom) {

            super(String.format(MESSAGE, nom));
        }


}
