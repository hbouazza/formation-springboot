package fr.dawan.formation.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MonumentNotFoundException extends RuntimeException{
    private final static String MESSAGE = "Le monument [%s] n'a pas été trouvé ";
    public MonumentNotFoundException(Long id) {
        super(String.format(MESSAGE, id));
    }
}
