package fr.dawan.formation.service;

import fr.dawan.formation.domain.Monument;
import fr.dawan.formation.domain.Point;
import fr.dawan.formation.exception.MonumentAlreadyExistingException;
import fr.dawan.formation.exception.MonumentNotFoundException;
import fr.dawan.formation.repository.MonumentRepository;
import net.sf.ehcache.CacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
class MonumentServiceImpl implements MonumentService{

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private MonumentRepository monumentRepository;

    MonumentServiceImpl(MonumentRepository monumentRepository) {
        this.monumentRepository = monumentRepository;
    }

    @Override
    public Monument findById(long id) throws Exception {
        if (!monumentRepository.existsById(id)){
            throw new MonumentNotFoundException(id);
        } else {
            return monumentRepository.findById(id).get();
        }
    }

    @Override
    public Optional<Monument> findByNom(String nom) throws Exception {
        if (monumentRepository.existsByNom(nom)) {
            return monumentRepository.findByNom(nom);
        }
        return Optional.empty();
    }


    @Override
    public List<Monument> findAll() throws Exception {
        List<Monument> monuments = new ArrayList<>();
        monumentRepository.findAll().forEach(monuments::add);
        if (monuments.isEmpty()){
            throw new Exception();
        }
        return monuments;
    }

    @Override
    public Page<Monument> findAll(Pageable p) throws Exception {
        cacheManager.getCache("test");
        return monumentRepository.findAll(p);
    }

    @Override
    public Monument save(Monument monument) throws Exception {
        if(monumentRepository.existsByNom(monument.getNom())){
           throw new MonumentAlreadyExistingException(monument.getNom());
        }
        return monumentRepository.save(monument);
    }

    @Override
    public Boolean delete(long id) throws Exception {
        if (!monumentRepository.existsById(id)){
            throw new Exception();
        } else {
           monumentRepository.deleteById(id);
           return true;
        }
    }

    @Override
    public void initData() {
        Monument eiffel =  new Monument(1, new String("Tour Eiffel"), "La célèbre tour de Paris", Arrays.asList(new Point(1,15.3, 18.6), new Point(2, 15.9,17.5)));
        Monument pyramide =  new Monument(2, new String("Pyramide de Khéops"), "Sur le plateau de Gizeh", Arrays.asList(new Point(3,70.6, 28.6), new Point(4, 11.3,97.5)));
        Monument arc =  new Monument(3, new String("Arc de triomphe"), "La victoire ou la mort", Arrays.asList(new Point(5,445.3, 23.6), new Point(6, 12.9,65.5)));
        Monument muraille =  new Monument(4, new String("Muraille de chine"), "Visible de loin", Arrays.asList(new Point(7,251.3, 47.6), new Point(8, 25.9,32.5)));
        Monument pise =  new Monument(5, new String("Tour de Pise"), "Penche toujours", Arrays.asList(new Point(9,302.3, 56.6), new Point(10, 33.9,62.5)));
        Monument chateau =  new Monument(6, new String("Chateau des Ducs de Bretagne"), "Une merveille nantaise", Arrays.asList(new Point(11,86.3, 33.6), new Point(12, 45.9,110.5)));
        Monument louvre =  new Monument(7, new String("Musée du Louvre"), "Encore une pyramide", Arrays.asList(new Point(13,27.3, 19.6), new Point(14, 92.9,220.5)));
        Monument place =  new Monument(8, new String("Place d'espagne"), "La place mondialement connue de Séville", Arrays.asList(new Point(15,65.3, 15.6), new Point(16, 20.9,652.5)));
        monumentRepository.save(eiffel);
        monumentRepository.save(pyramide);
        monumentRepository.save(arc);
        monumentRepository.save(muraille);
        monumentRepository.save(pise);
        monumentRepository.save(chateau);
        monumentRepository.save(louvre);
        monumentRepository.save(place);

    }

}
