package fr.dawan.formation.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

import fr.dawan.formation.domain.Format;
import fr.dawan.formation.domain.Image;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class TransformerService {

	@Value("${convert}")
	private String convert;
	
	
	public Image transform(Image base, Format f) throws IOException, InterruptedException {
		String commandTemplate = "%s \"%s\" %s \"%s\"";
		
		File fin = File.createTempFile("IMG",base.getOriginalFileName());
		FileOutputStream fout = new FileOutputStream(fin);
		fout.write(base.getContenu());
		fout.close();
		
		File fou = File.createTempFile("IMG",f.getExtension());
		
		StringBuilder builder = new StringBuilder();
		for (String propkey : f.getProprietes().keySet()) {
			String propvalue = f.getProprietes().get(propkey);
			builder.append(" -");
			builder.append(propkey);
			builder.append(" ");
			builder.append(propvalue);
		}
		
		String command = String.format(commandTemplate,
											convert,
											fin.getAbsolutePath(),builder.toString(),
											fou.getAbsolutePath());
		System.out.println(command);
		Process p = Runtime.getRuntime().exec(command);
		p.waitFor();
		
		Image img = new Image();
		img.setContenu(Files.readAllBytes(fou.toPath()));
		img.setFormat(f);
		img.setImageGroupId(base.getImageGroupId());
		img.setId(UUID.randomUUID().toString());
		return img;
	}
}
