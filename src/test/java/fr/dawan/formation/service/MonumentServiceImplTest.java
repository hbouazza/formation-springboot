package fr.dawan.formation.service;

import fr.dawan.formation.domain.Monument;
import fr.dawan.formation.domain.Point;
import fr.dawan.formation.exception.MonumentNotFoundException;
import fr.dawan.formation.repository.MonumentRepository;
import net.sf.ehcache.CacheManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Profile("TEST")
@RunWith(SpringRunner.class)
public class MonumentServiceImplTest {

    @Value("${page.default.size}")
    private String propsvalue;

    private final static Monument TESTING_MONUMENT_INSTANCE = new Monument(
            1,
            new String("Tour Eiffel"),
            "La célèbre tour de Paris",
            Arrays.asList(new Point(1, 15.3, 18.6), new Point(2, 15.9, 17.5))
    );

    @Autowired
    private MonumentService monumentService;

    @MockBean
    private CacheManager cacheManager;

    @MockBean
    private MonumentRepository monumentRepository;

    @TestConfiguration
    static class MonumentServiceImplTestContextConfiguration {

        @Bean
        public MonumentService monumentService() {
            return new MonumentServiceImpl(null);
        }
    }


    @Before
    public void setUp() throws Exception {

    }
    @Test
    public void findByNomTest() throws Exception {
        // given
        Mockito.when(monumentRepository.findByNom(anyString())).thenReturn(Optional.of(TESTING_MONUMENT_INSTANCE));
        Mockito.when(monumentRepository.existsByNom(anyString())).thenReturn(true);

        // when
        Monument m = monumentService.findByNom("Tour Eiffel").get();

        //then
        assertThat(m).isNotNull();
        assertThat(m).isEqualTo(TESTING_MONUMENT_INSTANCE);
        verify(monumentRepository, times(1)).findByNom("Tour Eiffel");
        verify(monumentRepository, times(1)).existsByNom("Tour Eiffel");
    }

    @Test
    public void getOneMonumentTest() throws Exception {
        // given
        given(monumentRepository.findById(anyLong())).willReturn(Optional.of(TESTING_MONUMENT_INSTANCE));
        given(monumentRepository.existsById(anyLong())).willReturn(true);

        //when
        Monument a = monumentService.findById(TESTING_MONUMENT_INSTANCE.getId());

        //then
        assertThat(a).isNotNull();
        assertThat(TESTING_MONUMENT_INSTANCE).isEqualTo(a);

        verify(monumentRepository, times(1)).findById(TESTING_MONUMENT_INSTANCE.getId());
        verify(monumentRepository, times(1)).existsById(TESTING_MONUMENT_INSTANCE.getId());
    }
    @Test(expected = MonumentNotFoundException.class)
    public void getOneMonumentFailTest() throws Exception {
        // given

        given(monumentRepository.existsById(anyLong())).willReturn(false);

        //when
        Monument a = monumentService.findById(TESTING_MONUMENT_INSTANCE.getId());

        //then
        assertThat(a).isNotNull();
        verify(monumentRepository, times(1)).existsById(TESTING_MONUMENT_INSTANCE.getId());
        verify(status()).isNoContent();
    }

    @Test
    public void createMonumentTest() throws Exception {
        // given
        given(monumentRepository.save(any(Monument.class))).willReturn(TESTING_MONUMENT_INSTANCE);

        //when
        Monument created = monumentService.save(TESTING_MONUMENT_INSTANCE);

        //then
        assertThat(created).isInstanceOf(Monument.class);
        assertThat(created).isEqualTo(TESTING_MONUMENT_INSTANCE);
        assertThat(created).isNotNull();
        verify(monumentRepository, times(1)).save(TESTING_MONUMENT_INSTANCE);

    }

    @Test
    public void findAllTest() throws Exception {
        // given
        List<Monument> expectedMonuments = new ArrayList<>();
        expectedMonuments.add(new Monument(1, new String("Tour Eiffel"), "La célèbre tour de Paris", Arrays.asList(new Point(1, 15.3, 18.6), new Point(2, 15.9, 17.5))));
        expectedMonuments.add(new Monument(2, new String("Arc de triomphe"), "La victoire ou la mort", Arrays.asList(new Point(3, 85, 185.6), new Point(4, 60.3, 178.5))));
        expectedMonuments.add(new Monument(3, new String("Muraille de chine"), "Fort Alamo", Arrays.asList(new Point(5, 43, 67), new Point(6, 86.9, 74.5))));
        List<Monument> testedMonuments = new ArrayList<>();
        given(monumentRepository.findAll()).willReturn(expectedMonuments);

        // when
        testedMonuments = monumentService.findAll();

        // then
        assertThat(testedMonuments.size()).isEqualTo(3);
        assertThat(testedMonuments).isEqualTo(expectedMonuments);
        verify(monumentRepository, times(1)).findAll();
    }

    @Test
    public void pagingAllTest() throws Exception {
        // given
        System.out.println(propsvalue);
        int DEFAULT_PAGE_SIZE = Integer.parseInt(propsvalue);
        List<Monument> expectedMonuments = new ArrayList<>();
        expectedMonuments.add(new Monument(1, new String("Tour Eiffel"), "La célèbre tour de Paris", Arrays.asList(new Point(1, 15.3, 18.6), new Point(2, 15.9, 17.5))));
        expectedMonuments.add(new Monument(2, new String("Arc de triomphe"), "La victoire ou la mort", Arrays.asList(new Point(3, 85, 185.6), new Point(4, 60.3, 178.5))));
        expectedMonuments.add(new Monument(3, new String("Muraille de chine"), "Fort Alamo", Arrays.asList(new Point(5, 43, 67), new Point(6, 86.9, 74.5))));
        Page<Monument> monumentPage = new PageImpl<>(expectedMonuments);
        Pageable p = PageRequest.of(0, DEFAULT_PAGE_SIZE);
        given(monumentRepository.findAll(any(Pageable.class))).willReturn(monumentPage);

        // when
        Page<Monument> testedMonuments = monumentService.findAll(p);

        // then
        assertThat(testedMonuments.getContent().size()).isEqualTo(DEFAULT_PAGE_SIZE);
        assertThat(testedMonuments.getContent()).isEqualTo(expectedMonuments);
        verify(monumentRepository, times(1)).findAll(p);
    }

    @Test(expected=Exception.class)
    public void deleteByIdTest() throws Exception {
        // given
        given(monumentRepository.existsById(anyLong())).willReturn(false);

        // when
        monumentService.delete(TESTING_MONUMENT_INSTANCE.getId());
        verify(monumentRepository, times(1)).existsById(TESTING_MONUMENT_INSTANCE.getId());
    }

}
